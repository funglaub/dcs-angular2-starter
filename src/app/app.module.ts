import { NgModule, LOCALE_ID, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { APP_REDUCERS, APP_EPICS, APP_TRANSLATIONS } from '@dcs/angular2-utils';

import { AppComponent } from './app.component';
import { routes } from './app.routes';

import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { greetingReducer } from './backend/greetings.reducer';
import { pingEpic } from './backend/ping.epic';
import { AppErrorHandler } from './app-error-handler';

import * as translationsEn from '../locale/en.json';
import * as translationsDe from '../locale/de-DE.json';

// Because https://github.com/angular/angular/issues/11402
export function wrapInFunctionBecauseAngularBug11402En() {
  return { name: 'en', translations: translationsEn };
}

export function wrapInFunctionBecauseAngularBug11402De() {
  return { name: 'de-DE', translations: translationsDe };
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    RouterModule.forRoot(routes, {
      useHash: false
    }),
    BrowserModule,
    CoreModule,
    SharedModule
  ],
  exports: [BrowserModule],
  providers: [
    { provide: LOCALE_ID, useValue: 'de-DE' },
    { provide: ErrorHandler, useClass: AppErrorHandler },
    { provide: APP_REDUCERS, useValue: { name: 'greeting', reducer: greetingReducer }, multi: true },
    { provide: APP_EPICS, useValue: pingEpic, multi: true },
    { provide: APP_TRANSLATIONS, useFactory: wrapInFunctionBecauseAngularBug11402En, multi: true },
    { provide: APP_TRANSLATIONS, useFactory: wrapInFunctionBecauseAngularBug11402De, multi: true }
  ]
})
export class AppModule {}
