import * as fs from 'fs';
import { Subject } from 'rxjs/Subject';
import { expect } from 'chai';
import { APP_BASE_HREF } from '@angular/common';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { LOCALE_ID } from '@angular/core';
import { RouterModule } from '@angular/router';
import { By } from '@angular/platform-browser';
import { NgReduxTestingModule, MockNgRedux } from '@angular-redux/store/testing';

import { TranslateModule } from '@ngx-translate/core';
import { Angular2UtilsModule, IState, APP_SETTINGS, APP_TRANSLATIONS } from '@dcs/angular2-utils';

import { CoreModule } from '../core/core.module';
import { HomeModule } from './home.module';
import { HomeComponent } from './home.component';
import { bangGreetingSelector } from './../backend/greetings.selectors';

describe('HomeComponent', () => {
  let subject: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(() => {
    TestBed.resetTestingModule();
    MockNgRedux.reset();

    TestBed.configureTestingModule({
      declarations: [],
      imports: [
        HomeModule,
        CoreModule,
        NgReduxTestingModule,
        RouterModule.forRoot([]),
        Angular2UtilsModule,
        TranslateModule.forRoot()
      ],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' },
        { provide: APP_SETTINGS, useValue: {} },
        { provide: LOCALE_ID, useValue: 'de-DE' },
        {
          provide: APP_TRANSLATIONS,
          useValue: { name: 'de-DE', translations: { 'HOME.HELLO': 'Hallo {{ value }}' } },
          multi: true
        }
      ]
    });

    TestBed.overrideComponent(HomeComponent, {
      set: {
        template: fs.readFileSync(__dirname + '/home.component.html').toString(),
        templateUrl: undefined
      }
    });

    fixture = TestBed.createComponent(HomeComponent);
    subject = fixture.debugElement.componentInstance;

    const whoStub: Subject<string> = MockNgRedux.getSelectorStub<IState, string>(bangGreetingSelector);
    whoStub.next('Welt !!!');

    fixture.detectChanges();
  });

  it('renders the greeting into the HTML', () => {
    const de = fixture.debugElement.query(By.css('div.greeting'));
    expect(de.nativeElement.textContent).to.equal('Hallo Welt !!!');
  });

  it('stores the correct value inside the who property', () => {
    expect(subject.who).to.equal('Welt !!!');
  });
});
