import { expect } from 'chai';

describe('my awesome website', function () {

  it('should do some chai assertions', function () {
    browser.url('https://www.google.de/');
    browser.setValue('input[name="q"]', 'DCS Fürth');
    browser.waitForVisible('h3.r');

    // browser.saveScreenshot('foo.png');
    expect(browser.getTitle()).to.include('dcs fürth');

    browser.click('h3.r a');

    // browser.pause(5000);
  });

});
