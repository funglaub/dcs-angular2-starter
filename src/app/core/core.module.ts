import { NgModule, Optional, SkipSelf } from '@angular/core';
import { APP_SETTINGS, Angular2UtilsModule, APP_ERROR_FORMATTERS } from '@dcs/angular2-utils';

import { settings } from '../settings';
import { SharedModule } from '../shared/shared.module';


function throwIfAlreadyLoaded(parentModule: any, moduleName: string) {
  if (parentModule) {
    throw new Error(`${moduleName} has already been loaded. Import Core modules in the AppModule only.`);
  }
}

export function errorFormattersFactory() {
  return {
    validateTheAnswer: () => `Please read "The Hitchhiker's Guide to the Galaxy" for the correct answer`
  };
}

@NgModule({
  imports: [
    Angular2UtilsModule,
    SharedModule
  ],
  exports: [
    Angular2UtilsModule
  ],
  declarations: [],
  providers: [
    { provide: APP_SETTINGS, useValue: settings },
    { provide: APP_ERROR_FORMATTERS, useFactory: errorFormattersFactory }
  ]
})
export class CoreModule {

  constructor( @Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }
}
