import { fromJS } from 'immutable';
import { expect } from 'chai';
import { IState } from '@dcs/angular2-utils';

import { greetingReducer } from './greetings.reducer';

describe('greetingReducer', () => {
  it('reacts to the PONG action', () => {
    const newState: IState = greetingReducer(undefined, { type: 'PONG' });
    const expectedState: IState = fromJS({
      who: 'DCS Fürth'
    });

    expect(expectedState).to.equal(newState);
  });

  it('reacts to the PING action', () => {
    const newState: IState = greetingReducer(undefined, { type: 'PING' });
    const expectedState: IState = fromJS({
      who: 'World'
    });

    expect(expectedState).to.equal(newState);
  });
});
