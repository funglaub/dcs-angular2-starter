import { ActionsObservable } from 'redux-observable';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/mapTo';
import { IAction } from '@dcs/angular2-utils';

// Dummy epic to show the functionality, remove after some real epics are added
// for epics docs see https://redux-observable.js.org/
export function pingEpic(action$: ActionsObservable<IAction>) {
  return action$
    .ofType('PING')
    .delay(10)
    .mapTo({ type: 'PONG' });
}
