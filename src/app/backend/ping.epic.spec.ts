import { expect } from 'chai';
import { Subject } from 'rxjs/Subject';
import { ActionsObservable } from 'redux-observable';
import { IAction } from '@dcs/angular2-utils';

import { pingEpic } from './ping.epic';

describe('pingEpic', () => {
  it('answers PING with PONG', done => {
    const obs: Subject<IAction> = new Subject();
    const actionsObservable: ActionsObservable<IAction> = new ActionsObservable(obs);
    const epic = pingEpic(actionsObservable);

    epic.subscribe((action: IAction) => {
      expect(action).to.eql({ type: 'PONG' });
      done();
    });

    obs.next({ type: 'PING' });
  });
});
