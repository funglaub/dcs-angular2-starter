import deDE from './de-DE.json';
import en from './en.json';

export const TRANSLATIONS: { [key: string]: { [key: string]: string } } = {
  'de-DE': deDE,
  'en': en
};
