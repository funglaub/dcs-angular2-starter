const webpackMerge = require('webpack-merge');
const webpack = require('webpack');
const DefinePlugin = require('webpack/lib/DefinePlugin');
const WebpackNotifierPlugin = require('webpack-notifier');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

const TS_VERSION = require('typescript').version;
const ENV = (process.env.ENV = process.env.NODE_ENV = 'development');

const helpers = require('./helpers');
const commonConfig = require('./webpack.common.js');
const buildPath = helpers.root('build', 'development');

module.exports = webpackMerge(commonConfig, {
  devtool: 'cheap-module-source-map',

  output: {
    path: buildPath,
    filename: '[name].bundle.js',
    sourceMapFilename: '[name].js.map',
    chunkFilename: '[name]-[id].chunk.js',
    devtoolModuleFilenameTemplate: function(info) {
      return 'file:///' + info.absoluteResourcePath;
    }
  },

  module: {
    loaders: [
      {
        test: /\.ts$/,
        loaders: [
          '@angularclass/hmr-loader',
          { loader: 'ts-loader', options: { transpileOnly: true } },
          'angular-router-loader?debug=false',
          'angular2-template-loader'
        ],
        exclude: [/\.(spec|e2e)\.ts$/]
      }
    ]
  },

  plugins: [
    new webpack.NamedModulesPlugin(),
    new FriendlyErrorsWebpackPlugin(),
    new ForkTsCheckerWebpackPlugin({
      watch: helpers.root('src/**/*.ts')
    }),

    new WebpackNotifierPlugin(),

    new DefinePlugin({
      ENV: JSON.stringify(ENV),
      TS_VERSION: JSON.stringify(TS_VERSION)
    }),

    new webpack.LoaderOptionsPlugin({
      debug: true
    })
  ],

  devServer: {
    port: 3000,
    host: '0.0.0.0',
    historyApiFallback: true,
    quiet: true, // only in in combination with FriendlyErrorsWebpackPlugin
    watchOptions: {
      ignored: /node_modules/
      // poll: 1000
    }
  }
});
