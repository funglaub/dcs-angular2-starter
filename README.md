# DCS Angular2 Blueprint

## Installation

```bash

git clone git@bitbucket.org:dcsfuerth/dcs-angular2-starter.git
cd dcs-angular2-starter
npm install # will throw warnings about missing python on windows - just ignore, still works
npm run watch:dev:hmr # or watch:dev for classic reload insted of HMR
```

Then open your browser at http://localhost:3000 to view the app.

As an alternative, the yarn package manager also is supported (and recommended).
So, if yarn is working on your machine, instead of `npm install` just do `yarn install` and live happily ever after.

To get the best development experience, also install

https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd

for a little time travel debugging, state dump/load ...


## First time setup

* Open config/webpack.prod.js and change the OfflinePlugin::ServiceWorker::cacheName setting to something unique
* Open tslint.json and change all the references to *dcs* (naming conventions) to a project/customer shortcut


## Mock backend

The project uses

https://github.com/typicode/json-server

to provide a fast, full featured mocked REST API.

If the project needs it, create the file

mock/db.json

in the project and edit it according to ther json-server docs.

Then, on second console, start the fake REST backend:

```bash
npm run backend:mock
```


## Polyfills

Depending on the browser matrix for the project, some polyfills may be needed for angular to run.

In the file

config/polyfills/polyfills.js

all necassary polyfills are activated and documented down to IE9.

Usually you can get away by just defining the required IE version in config/webpack.common.js.
Should you need something more complex, edit the polyfills file itself.


## Unit tests

The unit tests are based on mocha & chai, NOT on karma anymore.

Run once:

```bash
npm run test
```

Run during development:

```bash
npm run test:watch
```

Run with coverage:

```bash
npm run test:coverage
```

Then open coverage/index.html for the coverage report.


## Build for production

```bash
npm run build:prod # get coffee
npm run serve:prod # little test server for the production build
```

Then open your browser at http://localhost:8080 to view the app.
